package main

import (
	"crypto/x509"
	"fmt"
	"net/http"
	"os"
	"time"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("Check how long until SSL cert expires\n  usage: %s url\n", os.Args[0])
		return
	}
	now := time.Now()
	url := os.Args[1]
	res, err := http.Get("https://" + url + "/")
	if err != nil {
		panic(err)
	}
	var targetCert *x509.Certificate
	for _, i2 := range res.TLS.PeerCertificates {

		for _, name := range i2.DNSNames {
			if name == url {
				targetCert = i2
			}
		}
	}
	out := targetCert.NotAfter.Sub(now)
	fmt.Printf("%s cert expires in %v\n", targetCert.DNSNames, out)
	if out < time.Hour*720 {
		fmt.Println("cert is eligible to be renewed")
	} else {
		fmt.Printf("%v cert will be eligible for renewal in %v\n", targetCert.DNSNames, targetCert.NotAfter.Add(-time.Hour*720).Sub(now))
	}
}
